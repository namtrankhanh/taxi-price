import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import taxiApp from './reducers';
import SceneContainer from './components/SceneContainer';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';

const store = createStore(taxiApp, {
  search: {
    // start: {
    //   address: 'A',
    //   location: {
    //     latitude: 40.7127837,
    //     longitude: -74.0059413
    //   }
    // },
    // end: {
    //   address: 'B',
    //   location: {
    //     latitude: 37.7749295,
    //     longitude: -122.4194155
    //   }
    // },
    // zoomLocation: null
  },
  result: {}
}, applyMiddleware(
  createLogger(),
  thunk
));

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <SceneContainer></SceneContainer>
      </Provider>
    );
  }
}

export default App;