export const SHOW_SUGGESTIONS = 'SUGGESTION_CONTAINER/SHOW_SUGGESTIONS';
export const HIDE_SUGGESTIONS = 'SUGGESTION_CONTAINER/HIDE_SUGGESTIONS';


export function showSuggestions(items, config, callback) {
  return {
    type: SHOW_SUGGESTIONS,
    items,
    config,
    callback
  }
}

export function hideSuggestions() {
  return {
    type: HIDE_SUGGESTIONS
  }
}

export function selectItem() {
  return (dispatch, getState) => {
    let callback = getState().suggestionContainer.callback;
    dispatch(callback());
  }
}