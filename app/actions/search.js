export const SET_START_ADDRESS = 'SEARCH/SET_START_ADDRESS';
export const SET_END_ADDRESS = 'SEARCH/SET_END_ADDRESS';
export const SET_CURRENT_LOCATION = 'SEARCH/SET_CURRENT_LOCATION';
export const SET_ZOOM_LOCATION = 'SEARCH/SET_ZOOM_LOCATION';
export const CLEAR_START_ADDRESS = 'SEARCH/CLEAR_START_ADDRESS';
export const CLEAR_END_ADDRESS = 'SEARCH/CLEAR_END_ADDRESS';

export function setStartAddress(address, location) {
  return {
    type: SET_START_ADDRESS,
    address,
    location
  };
}

export function setEndAddress(address, location) {
  return {
    type: SET_END_ADDRESS,
    address,
    location
  };
}

export function setCurrentLocation(location) {
  return {
    type: SET_CURRENT_LOCATION,
    location
  }
}

export function setZoomLocation(location) {
  return {
    type: SET_ZOOM_LOCATION,
    location
  }
}

export function getAndSetCurrentPosition() {
  return (dispatch) => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let initialPosition = JSON.stringify(position);
        dispatch(setCurrentLocation(position));
      },
      (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );
  }

}

export function clearStartAddress() {
  return {
    type: CLEAR_START_ADDRESS
  }
}

export function clearEndAddress() {
  return {
    type: CLEAR_END_ADDRESS
  }
}

export function search() {
  
}

