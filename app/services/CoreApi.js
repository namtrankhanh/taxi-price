var Promise = require('promise/setimmediate');
import env from './Env';
console.log(env);
export function getPriceComparision(startAddress, endAddress) {

  return new Promise((resolve, reject) => {

    let url = env.API_URL;
    url += '/api/v1.0/result';
    url += '?origin_lat=' + startAddress.location.latitude;
    url += '&origin_lng=' + startAddress.location.longitude;
    url += '&origin_description=' + startAddress.address;
    url += '&destination_lat=' + endAddress.location.latitude;
    url += '&destination_lng=' + endAddress.location.longitude;
    url += '&destination_description=' + endAddress.address;

    console.log(url);

    fetch(url)
      .then(response => response.json())
      .then(data => {
        resolve(data.taxis)
      })
      .catch(err => reject(err));
  });

}
