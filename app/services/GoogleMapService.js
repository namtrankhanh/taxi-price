import { GOOGLE_MAP_API_KEY } from '../constants'
var Promise = require('promise/setimmediate');
const decodePolyline = require('decode-google-map-polyline');

/**
 * Get address suggestions by query
 *
 * @export
 * @param {any} query
 * @returns {Object[]} result - Array of address
 */
export function fetchLocationSuggestion(query) {
  return new Promise(function(resolve, reject) {
    let url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json';
    url += '?key=' + GOOGLE_MAP_API_KEY;
    url += '&input=' + query;
    url += '&language=vi';
    url += '&components=country:vn'

    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(data => {
        console.log(data);
        if (data.status !== 'OK') {
          return reject('Empty result');
        } else {
          return resolve(data.predictions);
        }
      })
      .catch(err => reject(err));
  })
}

export function getAddressByLocation(location) {
  return new Promise((resolve, reject) => {
    let url = 'https://maps.googleapis.com/maps/api/geocode/json';
    url += '?latlng=' + location.latitude + ',' + location.longitude;
    url += '&key=' + GOOGLE_MAP_API_KEY;
    fetch(url)
      .then(response => response.json())
      .then(data => {
        if (data.status === 'OK') {
          return resolve(data.results[0].formatted_address);
        }
        return reject('Can not resolve location to address');
      })
      .catch(err => reject(err));
  });
}

/**
 * Convert address string to LatLon geo location
 *
 * @export
 * @param {any} address
 * @returns {GeoLocation} result
 */
export function getPlaceDetails(placeId) {
  return new Promise((resolve, reject) => {
    let url = 'https://maps.googleapis.com/maps/api/place/details/json';
    url += '?key=' + GOOGLE_MAP_API_KEY;
    url += '&placeid=' + placeId;

    fetch(url)
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        if (data.status !== 'OK') {
          return reject(data.error_message);
        } else {
          return resolve(data.result);
        }
      })
      .catch(err => reject(err));
  })
}

/**
 * Get driving path
 */
export function getDrivingPath(location1, location2) {
  return new Promise((resolve, reject) => {
    let url = 'https://maps.googleapis.com/maps/api/directions/json';
    url += '?key=' + GOOGLE_MAP_API_KEY;
    url += '&origin=' + location1.latitude + ',' + location1.longitude;
    url += '&destination=' + location2.latitude + ',' + location2.longitude;
    url += '&mode=driving';
    url += '&avoid=indoor';
    url += '&language=vi';
    // console.log(url);
    fetch(url)
      .then(response => response.json())
      .then(data => {
        if (data.status !== 'OK' && data.routes.length === 0) {
          return reject(new Error('Empty data'));
        } else {
          let paths = [];
          data.routes[0].legs[0].steps.forEach(item => {
            let path = decodePolyline(item.polyline.points);
            path = path.map(item => {
              return {
                latitude: item.lat,
                longitude: item.lng
              }
            })
            paths = paths.concat(path);
          });
          return resolve(paths);
        }
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
  });
}