import _ from 'lodash';
let env = process.env.ENV;
let data = getEnvDataByEnvName(env);

console.log(data);
export default data;


function getEnvDataByEnvName(env) {
  let envData = {};
  if (env === 'staging') {
    envData = getStagingEnv();
  } else {
    envData = {};
  }
  return Object.assign({}, defaultEnv(), envData);
}

function defaultEnv() {
  return {
    API_URL: 'http://sosanhgiacuoc.com'
  };
}

function getStagingEnv() {
  return {
    API_URL: 'http://stg.sosanhgiacuoc.com'
  };
}

