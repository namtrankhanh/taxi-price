import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListView } from 'react-native';
import SuggestionPopupView from './SuggestionPopupView';

class SuggestionPopup extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      })
    }
  }

  componentWillReceiveProps(props) {
    console.log(props);
    if (props.items) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(props.items)
      });
    }
  }

  render() {
    return <SuggestionPopupView {...this.props} dataSource={this.state.dataSource} />
  }
}

const mapStateToProps = (state) => {
  return {
    isShowing: state.suggestionContainer.isShowing,
    items: state.suggestionContainer.items,
    selectItem: state.suggestionContainer.callback,
    position: state.suggestionContainer.config.position
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    //selectItem: (item) => dispatch(selectItem(item))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SuggestionPopup);