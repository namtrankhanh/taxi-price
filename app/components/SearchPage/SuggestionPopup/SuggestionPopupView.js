import React, { Component } from 'react';
import { ListView, View, Text, StyleSheet, TouchableOpacity } from 'react-native';
var dismissKeyboard = require('dismissKeyboard');

export default class SuggestionPopupView extends Component {

  constructor(props) {
    super(props);
    this.renderEmptyMessage = this.renderEmptyMessage.bind(this);
    this.renderSuggestionItem = this.renderSuggestionItem.bind(this);
  }

  render() {
    let { isShowing, dataSource } = this.props;
    return (
      <View style={[styles.container, this.containerStyle()]}>
        { isShowing &&
        <View>
          <ListView dataSource={dataSource} renderRow={this.renderSuggestionItem}></ListView>
          { this.props.items && this.props.items.length === 0 && this.props.loaded &&
            this.renderEmptyMessage()
          }
        </View>
        }
      </View>
    );
  }

  renderSuggestionItem(item) {
    let { selectItem } = this.props;
    return (
      <View style={styles.item}>
        <TouchableOpacity onPress={() => {
          dismissKeyboard();
          selectItem(item);
        }}>
          <Text style={styles.itemText}>{ item.description }</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderEmptyMessage() {
    return (
      <View style={styles.item}>
        <Text style={styles.itemText}>Không có kết quả</Text>
      </View>
    );
  }

  containerStyle() {
    if (!this.props.isShowing) {
      return {
        height: 0,
        backgroundColor: 'transparent'
      };
    } else {
      return {
        top: this.props.position.top,
        left: this.props.position.left,
        width: this.props.position.width,
        backgroundColor: '#247161'
      };
    }
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: 'green',
    borderRadius: 5,
    padding: 3,
    zIndex: 1
  },
  item: {
    padding: 3
  },
  itemText: {
    color: 'white'
  }
});
