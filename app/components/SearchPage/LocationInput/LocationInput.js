import React, { Component } from 'react';
import { fetchLocationSuggestion, getPlaceDetails } from '../../../services/GoogleMapService';
import { showSuggestions, selectItem, hideSuggestions } from '../../../actions/suggestion-container';
import { connect } from 'react-redux';
import LocationInputView from './LocationInputView';
import { isString } from 'lodash';

class LocationInput extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.clearText = this.clearText.bind(this);
    this.state = {
      query: null,
      listMarginTop: 0,
    };
  }

  componentWillReceiveProps(props) {
    if (props.text) {
      console.log(props);
      let text = props.text.length === '' ? null : props.text;
      this.setState({
        query: text
      });
    }
  }

  render() {
    return <LocationInputView
            ref={ref => this.view = ref}
            {...this.props}
            query={this.state.query}
            onChangeText={this.onChangeText}
            clearText={this.clearText}  />
  }

  selectItem(item) {
    
    this.props.hideSuggestions();

    getPlaceDetails(item.place_id)
      .then(details => {
    
        this.props.onSelectLocation(item.description, {
          latitude: details.geometry.location.lat,
          longitude: details.geometry.location.lng
        });
      });
  }

  componentDidMount() {
    setTimeout(() => {
      this.measureInputSizeAndPosition();
    }, 0);
  }

  measureInputSizeAndPosition() {
    this.view.refs.textInput.measure((fx, fy, width, height, px, py) => {
      console.log(fy, py, height);
      this.setState({
        suggestionTop: py + height,
        suggestionLeft: px,
        suggestionWith: width
      });
    });
  }

  clearText() {
    this.setState({
      query: null
    });
    this.props.onClear();
  }

  onChangeText(text) {
    this.setState({
      query: text != '' ? text : null
    });

    if (text === null || text === '') {
      this.props.hideSuggestions();
      return;
    }

    return fetchLocationSuggestion(text)
      .then(result => {
        this.props.showSuggestions(result, {
          position: {
            top: this.state.suggestionTop,
            left: this.state.suggestionLeft,
            width: this.state.suggestionWith
          }
        }, (item) => {
          this.setState({
            query: item.description
          });
          this.selectItem(item);
        });
      })
      .catch(err => {
        console.log(err);
        this.props.hideSuggestions();
      });
  }

}

const mapStateToProps = (state) => {
  return {

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    showSuggestions: (items, config, callback) => dispatch(showSuggestions(items, config, callback)),
    hideSuggestions: () => dispatch(hideSuggestions())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(LocationInput);