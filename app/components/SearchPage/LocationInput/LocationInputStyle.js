import { Platform, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  locationInputWrapper: {
    position: 'relative',
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  icon: {
    width: 20,
    marginLeft: 5,
  },
  textInput: {
    fontSize: 15,
    padding: (Platform.OS === 'android') ? 0 : 0,
    flexGrow: 1,
    marginRight: 5,
    height: (Platform.OS === 'android') ? 35 : 30,
  },
  removeIcon: {
    marginRight: 5
  }
});

export default styles;