import React, { Component } from 'react';
import { View, Alert, ListView, Text, TextInput, ListViewDataSource, TouchableHighlight, findNodeHandle } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './LocationInputStyle';

export default class LocationInputView extends Component {
  render() {
    let {
      onSelectLocation,
      text,
      placeholder,
      icon,
      hasBorder,
      onChangeText,
      query,
      clearText
    } = this.props;

    return (

      <View
        style={[styles.locationInputWrapper, {
                borderBottomWidth: hasBorder ? 1 : 0,
                borderBottomColor: '#acafae'
              }]}
              onStartShouldSetResponderCapture={e => {
                const target = e.nativeEvent.target;
                if (target !== findNodeHandle(this.refs.textInput)) {
                  this.refs.textInput.blur();
                }
              }}>

        <Icon name={icon} size={20} color={'black'} style={styles.icon}/>

        <TextInput
          ref="textInput"
          onChangeText={text => onChangeText(text)}
          value={query}
          clearButtonMode="while-editing"
          style={styles.textInput}
          placeholder={placeholder}
          underlineColorAndroid={'transparent'}
          clearButtonMode={'never'}
        />

        { query && query.length > 0 &&
          <TouchableHighlight onPress={() => clearText()}>
          <Icon name={'times'} size={10} color={'black'} style={styles.removeIcon} />
          </TouchableHighlight>
        }

      </View>

    );
  }
}
