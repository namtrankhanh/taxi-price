import React, { Component } from 'react';
import { connect } from 'react-redux';
import TripMapView from './TripMapView';
import { isEqual, isEmpty } from 'lodash';
import { getDrivingPath } from '../../../services/GoogleMapService';

export default class TripMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      paths: []
    }
  }

  componentWillReceiveProps(props) {
    if (props.zoomLocation && false === isEqual(props.zoomLocation, this.props.zoomLocation)) {
      this.refs.view.animateToCoordinate(props.zoomLocation);
    }

    if (props.startAddress && props.endAddress) {
      return getDrivingPath(props.startAddress.location, props.endAddress.location)
        .then(paths => {
          this.refs.view.setRegion([props.startAddress.location, props.endAddress.location]);

          this.setState({
            paths: paths
          });
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.setState({
        paths: null
      });
    }
  }

  render() {
    let { startAddress, endAddress, zoomLocation, height, children } = this.props;
    let { paths } = this.state;
    return (
      <TripMapView ref="view"
                   zoomLocation={zoomLocation}
                   startAddress={startAddress}
                   endAddress={endAddress}
                   height={height}
                   paths={paths}
                   >

            </TripMapView>
    )
  }

}
