import React, { Component } from 'react';
import MapView from 'react-native-maps';
import { View, StyleSheet, Text } from 'react-native';
import BlueFlag from '../../../../assets/flag-blue.png';
import PinkFlag from '../../../../assets/flag-pink.png';
import { isEqual } from 'lodash';

export default class TripMapView extends Component {

  constructor(props) {
    super(props);
    this.animateToCoordinate = this.animateToCoordinate.bind(this);
    this.setRegion = this.setRegion.bind(this);
  }

  animateToCoordinate(location) {
    this.map.animateToRegion({
      ...location,
      latitudeDelta: 0.0043,
      longitudeDelta: 0.0034
    });
  }

  setRegion(locationArray) {
    setTimeout(() => {
      let edgePadding = this.props.edgePadding ? this.props.edgePadding : this.getDefaultEdgePadding();
      this.map.fitToCoordinates(locationArray, {
        edgePadding: edgePadding,
        animated: true
      });
    }, 50)

  }

  getDefaultEdgePadding() {
    return {
      top: 100,
      bottom: 100,
      right: 100,
      left: 100
    }
  }

  render() {
    let { startAddress, endAddress, paths } = this.props;
    let path = [];
    if ( startAddress && endAddress ) {
      path = [startAddress.location, endAddress.location];
    }

    return (
      <View style={styles.container}>
          <MapView style={styles.map} ref={ref => this.map = ref}>
            { startAddress &&
              <MapView.Marker
                title={this.shortenAddress(startAddress.address)}
                coordinate={ startAddress.location }>
              </MapView.Marker>
            }

            { endAddress &&
              <MapView.Marker
                title={this.shortenAddress(endAddress.address)}
                coordinate={endAddress.location}>
              </MapView.Marker>
            }

            {
              paths && paths.length > 0 &&
              <MapView.Polyline
                coordinates={paths}
              />
            }

          </MapView>

        </View>
    )
  }

  shortenAddress(address) {
    if (address) {
      let arr = address.split(',');
      return arr[0].trim();
    }
    return null;

  }

}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1,
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    ...StyleSheet.absoluteFillObject,
  },
});