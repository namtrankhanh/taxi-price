import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity, ActivityIndicator, ScrollView, TextInputState } from 'react-native';
import NavigationBar from 'react-native-navbar';
import LocationInput from './LocationInput/LocationInput';
import TripMap from './TripMap/TripMap';
import SuggestionPopup from './SuggestionPopup/SuggestionPopup';
import Spinner from './Spinner/Spinner';

class SearchPageView extends Component {

  render() {
    let {
      setStartAddress,
      setEndAddress,
      startSearch,
      startAddress,
      endAddress,
      zoomLocation,
      isLoading,
      clearStartAddress,
      clearEndAddress
    } = this.props;

    return (
      <View style={styles.searchPageWrapper}>
        <SuggestionPopup />

        <Spinner isShowing={isLoading} text={'Kiểm tra vị trí của bạn...'}/>

        <NavigationBar
          title={{title: "So Sánh Giá Taxi", tintColor: 'white'}}
          tintColor={'#095894'} />

        <View style={styles.searchPage}>
          <View style={styles.tripMapView}>

            <View style={styles.searchForm}>
              <LocationInput
                onSelectLocation={setStartAddress}
                onClear={clearStartAddress}
                text={startAddress ? startAddress.address : null}
                placeholder="Nơi xuất phát"
                icon={'location-arrow'}
                hasBorder={true}/>

              <LocationInput
                onSelectLocation={setEndAddress}
                onClear={clearEndAddress}
                text={endAddress ? endAddress.address : null}
                placeholder="Điểm đến"
                icon={'map-marker'} />
            </View>

            <TripMap
              startAddress={startAddress}
              endAddress={endAddress}
              zoomLocation={zoomLocation}
              edgePadding={
                {
                  top: 350,
                  bottom: 200,
                  left: 100,
                  right: 100,
                }
              } />

            <TouchableOpacity onPress={() => startSearch()} style={styles.searchTouchable}>
              <View style={styles.searchButton}>
                <Text style={styles.searchText}>Tìm giá taxi</Text>
              </View>
            </TouchableOpacity>

          </View>


        </View>

      </View>
    )
  }

}

const styles = StyleSheet.create({
  searchPageWrapper: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: 'stretch',
  },
  searchPage: {
    flex: 1,
    alignItems: 'stretch',
    position: 'relative',
    flexDirection: 'column'
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    alignSelf: 'center',
    textAlign: 'center',
    padding: 5
  },
  tripMapView: {
    position: 'relative',
    flex: 1,
    flexGrow: 1
  },
  searchForm: {
    position: 'absolute',
    flex: 1,
    alignSelf: 'stretch',
    zIndex: 10,
    left: 0,
    right: 0,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#bfbfbf'
  },
  searchTouchable: {
    position: 'absolute',
    bottom: 10,
    paddingTop: 10,
    alignSelf: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    left: 0,
    right: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#bfbfbf'
  },
  searchButton: {
    zIndex: 1,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#095894',
  },
  searchText: {
    color: 'white',
    textAlign: 'center'
  },
});

export default SearchPageView;