import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NetInfo } from 'react-native';
import { setStartAddress, setEndAddress, search, setZoomLocation, clearStartAddress, clearEndAddress } from '../../actions/search';
import { clearResult } from '../../actions/result';
import SearchPageView from './SearchPageView';
import ResultPage from '../ResultPage/ResultPage';
import { getAddressByLocation } from '../../services/GoogleMapService';

class SearchPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    }
    this.startSearch = this.startSearch.bind(this);
    this.validateSearchData = this.validateSearchData.bind(this);
  }

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        alert('Vui lòng kiểm tra lại kết nối internet.');
        return null;
      } else {
        this.getCurrentPosition();
      }
    });
  }

  getCurrentPosition() {
    this.setState({
      isLoading: true
    });

    navigator.geolocation.getCurrentPosition(
      (position) => {
        var initialPosition = JSON.stringify(position);
        let location = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        }
        getAddressByLocation(location)
          .then(address => {
            this.setState({
              isLoading: false
            });
            this.props.setStartAddress(address, location);
          });
      },
      (error) => {
        alert('Bật GPS để lấy vị trí hiện tại của bạn');
        this.setState({
          isLoading: false
        });
        console.log(error);
      },
      {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000}
    );
  }

  render() {
    return (
      <SearchPageView
        {...this.props}
        startSearch={this.startSearch}
        isLoading={this.state.isLoading}></SearchPageView>
    )
  }

  validateSearchData() {
    let hasError = false;

    this.setState({
      invalidStartAddress: false,
      invalidEndAddress: false
    })

    if (!this.props.startAddress) {
      this.setState({
        invalidStartAddress: true
      });
      hasError = true;
    }

    if (!this.props.endAddress) {
      this.setState({
        invalidEndAddress: true
      })
      hasError = true;
    }

    return hasError === false;
  }

  startSearch() {
    let ok = this.validateSearchData();
    if (ok) {
      this.props.clearResult();
      this.props.navigator.push({
        component: ResultPage
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    startAddress: state.search.start,
    endAddress: state.search.end,
    zoomLocation: state.search.zoomLocation
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setStartAddress: (address, location) => {
      dispatch(setStartAddress(address, location));
      dispatch(setZoomLocation(location));
    },
    clearStartAddress: () => {
      dispatch(clearStartAddress());
    },
    setEndAddress: (address, location) => {
      dispatch(setEndAddress(address, location));
      dispatch(setZoomLocation(location));
    },
    clearEndAddress: () => {
      dispatch(clearEndAddress());
    },
    setZoomLocation: (location) => dispatch(setZoomLocation(location)),
    search: () => dispatch(search()),
    clearResult: () => dispatch(clearResult())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);