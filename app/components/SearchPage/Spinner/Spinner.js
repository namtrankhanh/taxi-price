import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, Text } from 'react-native';

export default class Spinner extends Component {
  render() {
    let { isShowing, text } = this.props;
    return (
      <View style={[styles.container, this.containerStyle()]}>
        { isShowing &&
          <View style={styles.spinerWrapper}>
            <ActivityIndicator
              style={styles.activityIndicator}
              size="large"
              color="#aa00aa"/>
            { text &&
              <Text style={styles.text}>{text}</Text>
            }
          </View>
        }
      </View>
    )
  }

  containerStyle() {
    if (!this.props.isShowing) {
      return {
        width: 0,
        height: 0
      };
    } else {
      return {};
    }
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1000,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
  spinerWrapper: {
    zIndex: 1001,
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    borderRadius: 5,
    padding: 10

  },
  activityIndicator: {
    padding: 8,
    alignSelf: 'center',
  },
  text: {
    fontSize: 15,
  }
})