import React, { Component } from 'react';
import { Navigator } from 'react-native';
import SearchPage from './SearchPage/SearchPage';
import ResultPage from './ResultPage/ResultPage';

class SceneContainer extends Component {

  configureScene() {
    return {
      ...Navigator.SceneConfigs.PushFromRight,
      gestures: {}
    };
  }

  renderScene(route, navigator) {
    if (route.component) {
      return React.createElement(route.component, { navigator, ...route.passProps });
    }
  }

  render() {
    return (
      <Navigator
        initialRoute={{name: 'Search', component: SearchPage}}
        configureScene={this.configureScene}
        renderScene={this.renderScene}
      />
    );
  }
}

export default SceneContainer;