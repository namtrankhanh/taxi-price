import React, { Component } from 'react';
import { connect } from 'react-redux';
import { search } from '../../actions/result';
import ResultPageView from './ResultPageView';
import SearchPage from '../SearchPage/SearchPage';

class ResultPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      result: []
    }
    this.back = this.back.bind(this);
  }

  componentDidMount() {
    this.props.search();
  }

  render() {
    let { isSearching, result, startAddress, endAddress } = this.props;
    return <ResultPageView
      result={result}
      startAddress={startAddress}
      endAddress={endAddress}
      isSearching={isSearching}
      back={this.back}/>
  }

  back() {
    this.props.navigator.pop();
  }
}

const mapStateToProps = (state) => {
  return {
    isSearching: state.result.isSearching,
    result: state.result.result,
    startAddress: state.search.start,
    endAddress: state.search.end
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    search: () => dispatch(search())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultPage);