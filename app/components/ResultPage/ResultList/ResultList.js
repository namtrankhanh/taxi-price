import React, { Component } from 'react';
import { ListView, Linking, Alert } from 'react-native';
import { connect } from 'react-redux';
import ResultListView from './ResultListView';
import callTaxi, { taxiCallable } from '../../../helpers/CallTaxi';

class ResultList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      }),
      loaded: false
    }
  }

  componentWillReceiveProps(props) {
    let dataSource = this.state.dataSource.cloneWithRows(props.list)
    this.setState({
      dataSource: dataSource,
      loaded: true
    });
  }

  render() {
    return (
      <ResultListView
        dataSource={this.state.dataSource}
        makeBooking={this.makeBooking.bind(this)} />
    )
  }

  makeBooking(taxi) {
    if (taxi.deep_link) {
      Linking
        .canOpenURL(taxi.deep_link)
        .then(supported => {
          if (!supported) {
            alert('Không mở được ứng dụng đặt xe, vui lòng kiểm tra cài đặt trên máy của bạn');
          } else {
            Linking.openURL(taxi.deep_link);
          }
        })
        .catch(err => {
          console.log(err);
          alert('Có lỗi khi mở app đặt xe');
        })
    } else if (taxi.phone) {
      let error = callTaxi(taxi.phone);
      if (error) {
        alert(error.message);
      }
    } else {
      alert('Hãng taxi này chưa được hỗ trợ');
    }
  }
}

const mapStateToProps = (state) => {
  return {
    list: state.result.list
  }
}

export default connect(mapStateToProps, null)(ResultList);