import React, { Component } from 'react';
import { ListView, View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { formatMoney } from '../../../helpers/Number';

import maiLinh from '../../../../assets/taxi-logo/mai-linh.png';
import vinaSun from '../../../../assets/taxi-logo/vina-sun.png';
import uber from '../../../../assets/taxi-logo/uber.jpg';
import grab from '../../../../assets/taxi-logo/grab.png';

export default class ResultListView extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let { dataSource } = this.props;
    return (
      <View style={styles.container}>
        <ListView
          dataSource={dataSource}
          renderRow={this.renderRow.bind(this)}
        />
      </View>
    )
  }

  renderRow(item) {
    return (
      <View style={styles.item}>
        { this.renderImage(item) }
        { this.renderName(item) }
        { this.renderPrice(item) }
        { this.renderBookingLink(item) }
      </View>
    )
  }

  renderImage(item) {
    let image = this.getImageByType(item.type);

    return (
      <View style={styles.imageWrapper}>
        <Image style={styles.image} source={image} />
      </View>
    )
  }

  renderName(item) {
    return (
      <View style={styles.nameWrapper}>
        <Text>{item.display_name}</Text>
      </View>
    )
  }

  renderPrice(item) {
    return (
      <View style={styles.priceWrapper}>
        <Text>{ formatMoney(item.price) } đ</Text>
        <Text style={styles.priceHintText}>
          {
            item.deep_link ? 'Giá real-time' : null
          }
        </Text>
      </View>
    )
  }

  renderBookingLink(item) {
    return (
      <View style={styles.booking}>
        <TouchableOpacity
          style={styles.bookingTouchable}
          onPress={() => this.props.makeBooking(item)}>
          <Text style={styles.bookingText}>Gọi xe</Text>
        </TouchableOpacity>
      </View>
    )
  }

  getImageByType(type) {
    if (type === 'UBER') {
      return uber;
    } else if (type === 'GRAB') {
      return grab;
    } else if (type === 'MAILINH') {
      return maiLinh;
    } else if (type === 'VINASUN') {
      return vinaSun;
    }
  }


}

const styles = StyleSheet.create({
  container: {
    padding: 0
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    height: 70,
    marginBottom: 0,
    borderBottomWidth: 1,
    borderBottomColor: '#bfbfbf'
  },
  imageWrapper: {
    width: 90,
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  image: {
    flex: 1,
    width: null,
    height: null
  },
  nameWrapper: {
    width: 100,
    paddingLeft: 10,
    justifyContent: 'center'
  },
  priceWrapper: {
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'center',
  },
  priceHintText: {
    fontSize: 12
  },
  booking: {
    alignItems: 'flex-end',
    flex: 0.2,
    justifyContent: 'center',
    padding: 5
  },
  bookingTouchable: {
    borderBottomWidth: 1,
    borderBottomColor: '#bfbfbf',
    backgroundColor: '#095894',
    padding: 15,
    borderRadius: 5
  },
  bookingText: {
    color: 'white'
  }
})