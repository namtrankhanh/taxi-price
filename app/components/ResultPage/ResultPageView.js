import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, ActivityIndicator, Text, TouchableOpacity } from 'react-native';
import TripMap from '../SearchPage/TripMap/TripMap';
import ResultList from './ResultList/ResultList';
import NavigationBar from 'react-native-navbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spinner from '../SearchPage/Spinner/Spinner';

export default class ResultPageView extends Component {

  constructor(props) {
    super(props);
    this.renderBackButton = this.renderBackButton.bind(this);
  }

  render() {
    let { isSearching, result, startAddress, endAddress } = this.props;
    return (
      <View style={styles.container}>
       <Spinner isShowing={isSearching} text="Kiểm tra giá taxi"/>

        <NavigationBar
          title={{title: "So Sánh Giá Taxi", tintColor: 'white'}}
          tintColor={'#095894'}
          leftButton={this.renderBackButton()}/>

        <View style={styles.tripMap}>
          <TripMap
            style={styles.tripMap}
            startAddress={startAddress}
            endAddress={endAddress}
          />
        </View>

        <ScrollView>
        <ResultList />
        </ScrollView>
      </View>
    )
  }

  renderBackButton() {
    return (
      <TouchableOpacity onPress={() => this.props.back()} style={styles.backButton}>
        <Icon name={'chevron-left'} size={25} color={'white'} />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  tripMap: {
    height: 200
  },
  backButton: {
    marginTop: 10,
    marginLeft: 5
  }
});