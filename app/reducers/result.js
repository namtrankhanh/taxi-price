import { SET_RESULT, IS_SEARCHING, CLEAR_RESULT } from '../actions/result';

export default function result(state = {list: [], isSearching: false}, action) {
  switch (action.type) {

    case SET_RESULT:
      return Object.assign({}, state, {
        list: action.list
      });

    case IS_SEARCHING:
      return Object.assign({}, state, {
        isSearching: action.value
      });

    case CLEAR_RESULT:
      return Object.assign({}, state, {
        list: [],
        isSearching: false
      });

    default:
      return state;
  }
}