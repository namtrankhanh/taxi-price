import {SHOW_SUGGESTIONS, HIDE_SUGGESTIONS} from '../actions/suggestion-container';

export default function suggestionContainer(state = {
  items: [],
  isShowing: false,
  config: {},
  callback: () => {}
}, action) {

  switch (action.type) {

    case SHOW_SUGGESTIONS:
      return Object.assign({}, state, {
        items: action.items,
        isShowing: true,
        config: action.config,
        callback: action.callback
      });

    case HIDE_SUGGESTIONS:
      return Object.assign({}, state, {
        isShowing: false
      });

    default:
      return state;
  }
}