import { combineReducers } from 'redux';
import search from './search';
import result from './result';
import suggestionContainer from './suggestion-container';

const taxiApp = combineReducers({
  search,
  result,
  suggestionContainer
});

export default taxiApp;