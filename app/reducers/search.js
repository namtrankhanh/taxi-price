import { SET_START_ADDRESS, SET_END_ADDRESS, SET_ZOOM_LOCATION, CLEAR_START_ADDRESS, CLEAR_END_ADDRESS } from '../actions/search';

export default function search(state = {
  start: {
    address: 'A',
    location: {
      latitude: 40.7127837,
      longitude: -74.0059413
    }
  },
  end: {
    address: 'B',
    location: {
      latitude: 37.7749295,
      longitude: -122.4194155
    }
  },
  zoomLocation: null
}, action) {
  switch (action.type) {

    case SET_START_ADDRESS:
      return Object.assign({}, state, {
        start: {
          address: action.address,
          location: action.location
        }
      });

    case SET_END_ADDRESS:
      return Object.assign({}, state, {
        end: {
          address: action.address,
          location: action.location
        }
      });

    case SET_ZOOM_LOCATION:
      return Object.assign({}, state, {
        zoomLocation: action.location
      });

    case CLEAR_START_ADDRESS:
      return Object.assign({} ,state, {
        start: null
      });

    case CLEAR_END_ADDRESS:
      return Object.assign({}, state, {
        end: null
      });

    default:
      return state;
  }

}