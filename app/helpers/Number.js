export function formatMoney(value, decimals = 0) {
  return value.toFixed(decimals).replace(/(\d)(?=(\d{3})+\b)/g,'$1,');
}