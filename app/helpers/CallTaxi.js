import { Linking, Platform } from 'react-native';
var SendIntentAndroid = require('react-native-send-intent');

export default function callTaxi(number) {
  if (number === null) {
    return new Error('Không tìm được số điện thoại');
  }
  if (Platform.OS === 'android') {
    SendIntentAndroid.sendPhoneCall(number);
    return;
  }

  if (Platform.OS === 'ios') {
    Linking.openURL('tel:' + number);
    return;
  }
}
